/**
 * Открывает или закрывает форму отзыва вместе с подложкой
 */
const handleToggleReviewModal = (e) => {
  const reviewModal = document.getElementById('modal-container-review-id');
  reviewModal.classList.toggle('modal-container-review_closed');
}

/**
 * Закрывает форму с благодарственным сообщением
 */
const handleToggleSuccessModal = (e) => {
  const successModal = document.getElementById('modal-container-success-id');
  successModal.classList.toggle('modal-container-success_closed');
}

/**
 * Обработка отправки формы
 */
const handleSubmitForm = (event) => {
  event.preventDefault();
  // ... отправка формы
  handleToggleReviewModal();
  handleToggleSuccessModal();
}

// Добавление обработчиков открытия формы на кнопки
const openReviewFormButtons = document.querySelectorAll('.open-review-form-btn');
openReviewFormButtons.forEach((buttonElement) => {
  buttonElement.addEventListener('click', handleToggleReviewModal)
});

// Закрытие формы отзыва
const modalReviewCloseBtn = document.getElementById("modal-review-close-btn");
modalReviewCloseBtn.addEventListener('click', handleToggleReviewModal);

// Добавление обработчика отправки формы отзыва
const reviewForm = document.getElementById('review-form-id');
reviewForm.addEventListener('submit', handleSubmitForm);

// Закрытие окна с благодарственным сообщением
const modalSuccessCloseBtn = document.getElementById('modal-success-close-btn');
const modalSuccessConfirmBtn = document.getElementById('modal-success-confirm-btn');
modalSuccessCloseBtn.addEventListener('click', handleToggleSuccessModal);
modalSuccessConfirmBtn.addEventListener('click', handleToggleSuccessModal);